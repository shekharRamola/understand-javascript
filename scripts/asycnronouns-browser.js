//here when the long running function will keep executing , so  the browser will put the click event in the event queue and will wait for the function to finish and after that it will execute the click function.
//

function waitForThreeSeconds(){
    var ms = 3000+ new Date().getTime();
    while(new Date() < ms ) { }
    console.log('finished  execution');
}
function clickHandler(){
    console.log('click event');
}
document.addEventListener('click', clickHandler)
waitForThreeSeconds();
console.log('finished execution')
