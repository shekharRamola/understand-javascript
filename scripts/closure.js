// closure is basically a feature of javascript which provide an environment where
// outer for inner function,outer function variable is available for them

function buildFunction() {
  var arr = [];
  for (var i = 0; i < 3; i++) {
    arr.push(function() {
      console.log(i);
    });
  }
  return arr;
}

var closure1 = buildFunction();
closure1[0](); //output will be 3 because of closure
closure1[1](); //output will be 3 because of closure
closure1[2](); //output will be 3 because of closure

//output is 3 because the function inside arr.push will only get executed when it is called and it will get
//called at the last line by line when the value of var reached to the 3. to solve this problem we can use
//let which will retain the value

function buildFunctionUsingLet() {
  var arr = [];
  for (let i = 0; i < 3; i++) {
    arr.push(function() {
      console.log(i);
    });
  }
  return arr;
}

var closure1Let = buildFunctionUsingLet();
closure1Let[0](); //output will be 0 because of closure
closure1Let[1](); //output will be 1 because of closure
closure1Let[2](); //output will be 2 because of closure

//we can also solve this problem using vanilla js by making an anonymous function
function buildFunctionUsingVanilla() {
  var arr = [];
  for (var i = 0; i < 3; i++) {
    arr.push((function(j) {
      // console.log(j);
      return function() {
        console.log(j);
      };
    })(i));
  }
  return arr;
}

var closure1Vanilla = buildFunctionUsingVanilla();
console.log(closure1Vanilla);
closure1Vanilla[0](); //output will be 2
closure1Vanilla[1](); //output will be 2
closure1Vanilla[2](); //output will be 2






// one more example of closure

function greet(whatToSay){
  return function(name) {
    console.log(whatToSay+ '' + name);
  }
}

greet('hi')('tony');




// below is an example of function factories which pattern can be make using closure.

function greetingFactory(greetMsg) {
  return function(firstName, lastName) {
    console.log(greetMsg+' '+ firstName+ ' ' +lastName);
  }
}

var greetInEng = greetingFactory('hello');
var greetInSpanish = greetingFactory('holla');

greetInEng('Shekhar', "Ramola");
greetInSpanish('Shekhar', "Ramola");
