//  it will return true althought it should be false because of associativity and coersion that is first
//  because '<' is left to right associativity therefore expression 3<2 will evaluate which will return false
//  now it will be false<1. so here javascript coerse false into number which is 0 so it will become
//  0<1 which is true hence the result.
console.log(3 < 2 < 1);
console.log(Number(undefined));
// return NaN;
console.log(Number(null));
//return 0;
