
console.log('fnction constructor initialised');



function Person(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}

Person.prototype.getFullName = function(){
    return this.firstname+' '+this.lastname;
}

var john= new Person('john', 'doe');

Person.prototype.getName = function(){
    return this.lastname+' '+this.firstname;
}
console.log(john);

console.log(john.getName());

console.log(john.getFullName());





var jane= new Person('jane', 'doe');
console.log(jane);
