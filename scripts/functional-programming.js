console.log('functional programming................................................');



console.log('naive way of mapping array');


let arr1 = [1,2,3];
let arr2 = [];
for (let index = 0; index < arr1.length; index++) {
    const element = arr1[index];
    arr2.push(element*2); 
}
console.log(arr2);

console.log('using functional programming approach.....using fist class functions ....');
function mapArray(array,fn){
    let newArrray = [];
    for (let index = 0; index < array.length; index++) {
        const element = array[index];
        newArrray.push(fn(element));
    }
    return newArrray;
}

let arr3 = mapArray(arr1, function(item) {
   return item*2 
});

console.log(arr3);

let arr4 = mapArray(arr1,function(item){
    return item>2;
}

)
console.log(arr4);

// function limitBy(limiter, item){
// return item> limiter;
// }
var limitBy = function(limiter,item){
    return item > limiter;
}

let arr5 = mapArray(arr1, limitBy.bind(this,2));
console.log(arr5);

// function genericLimitBy(limiter,item){
//     return function(){
//       return item>limiter;
//     }.bind(limiter)
// }
var genericLimitBy = function(limiter){
    return function(limiter,item){
        return item>limiter;
    }.bind(this, limiter);
}


const arr7 = mapArray(arr1, genericLimitBy(2));
console.log(arr7);

const arr6 = genericLimitBy(arr1, )