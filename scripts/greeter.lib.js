;(function(global, $){


    // 'new' is an object
    var Greetr = function(firstName, lastName, language){
        return new Greetr.init(firstName, lastName, language)
    }

    //hidden within the scope of the iife and never directly accessible
    var supportedLangs = ['en', 'es'];

    //informal greetings

    var greetings = {
en:'hello',
es: 'hola'
    };

    //formal greetings
    var formalGreetings = {
        en: 'Greetings',
        es: 'Saludos'
    };

    var logMessages = {
        en:'logged in',
        es:'Saludos'
    };

    Greetr.prototype = {
        fullName: function(){
            return this.firstName + ' ' + this.lastName
        },
        validate: function(){
            if(supportedLangs.indexOf(this.language) === -1){
                throw 'Invalid language';
            }
        },
        greeting: function(){
            return greetings[this.language]+ ' ' + this.firstName+ '!';
        },
        formalGreetings: function(){
            return formalGreetings[this.language] + ', ' + this.fullName();
        },
        greet: function(formal){
            let msg;
            if (formal) {
                msg = this.formalGreetings();
            } else {
                msg = this.greeting();
            }

            if(console){
                console.log(msg);
            }
            return this;
        },
        log: function(){
            if (console) {
                console.log(logMessages[this.language]+ ': '+ this.fullName());
            }

            return this;
        },
        setLang: function(lang){
            this.language = lang;
            this.validate();
            return this;
        },
        HTMLGreeting: function(selector,formal){
            if (!$) {
                throw 'JQUERY not loaded';
            }

            if (!selector) {
                throw 'missing jquery selector';
            }

            var msg;
            if (formal) {
                msg = this.formalGreetings()
            } else {
                msg = this.greeting();
                
            }
            $(selector).html(msg);

            return this;
        }

    };

    Greetr.init = function(firstName, lastName, language){
        var self = this;
        self.firstName = firstName || '';
        self.lastName = lastName || '';
        self.language = language || 'en';
    }
    Greetr.init.prototype = Greetr.prototype;



    global.Greetr = global.G$ = Greetr;


}(window, jQuery))