// Understand hoisting
console.log('initialising hoisting.js..............');


a();
console.log(b);


var b= 'hello';
function a(){
    console.log('a is called');
}


//gets a new object( this architecture allow us to not have to use the 'new' keyword here)
var g = G$('Joan', 'Doe');
console.log(g);


// use our chainable methods
g.greet().setLang('es').greet(true).log();



//let us use our object on the click of the login button
$('#login').click(function(){


    // create a new 'Greetr' object (let us pretend we know the name from the login)
    var loginGrtr = G$('john', 'doe');

    // hide the login on the screen
    $('#logindiv').hide();


    // fire off an html greeting, passing the '#greeting' as the selector and the chosen language, and log the welcome as well
    loginGrtr.setLang($('#lang').val()).HTMLGreeting('#greeting',true).log();
})



