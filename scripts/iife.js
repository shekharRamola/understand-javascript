/* different ways to make IIFE (basially there are two) */





/* this is an example of function statement */
function greet(name){
    console.log('hey buddy,' +name);
}

greet('Shekhar');


/* this is an example of function expression. It is one of the way to create IIFE */

var greeting = function(name){
    console.info('hey buddy, ' +name)
}('Shekhar');

/* this is the most efficient way to create an IIFE */


//(function(name) {
//    console.log('hey buddy, ' +name);
//})('Shekhar');

(function (name) {
   console.log('hey buddy, ' +name);
})('Shekhar');