// here we will learn about call, apply, bind and currying



// bind create copy of whatever function you are calling it on with the parameter as an passed object but not actually call it while apply and call does


// call, applly and bind
console.log('other.js...................................')

let person = {

    firstName: 'Shekhar',
    lastName: 'Ramola',
    showFullName: function(){
      //  console.log(this.firstName + ' ' + this.lastName);
      let fullName = this.firstName + ' ' + this.lastName;
      return fullName;
    }
}

 console.log(person.showFullName());   // I can access here when not inside fucntion

let logName = function(lang1, lang2){
    console.log('logged in ' + this.showFullName());
    console.log('language1 ' +lang1);
    console.log('language2 ' +lang2);
}
 let logPersonName = logName.bind(person);
 logPersonName('en', 'es');

 console.log('using call ');

 logName.call(person, 'en', 'es');


 console.log('using apply ');
 logName.apply(person, ['en', 'es']);


 console.log('function on fly');

 (function(lang1, lang2){
    console.log('logged in ' + this.showFullName());
    console.log('language1 ' +lang1);
    console.log('language2 ' +lang2);
}).apply(person, ['en', 'es']);





console.log('example of function borrowing below');

let person2 = {
    firstName:'john',
    lastName:'doe'
};


console.log(person.showFullName.bind(person2)());

console.log(person.showFullName.apply(person2));



console.log('example of function currying');


function multiply(a,b){
    return a*b;
}
const multiplyByTwo = multiply.bind(this,2);
console.log(multiplyByTwo(6));
const multiplyByThree = multiply.bind(this,3);
console.log(multiplyByThree(6));
