// understanding the prototype

console.log('initializing prototype.js.......................');

var personA = {
    firstName: 'Default',
    lastName: 'Default',
    getFullName: function(){
        return this.firstName+ ' '+ this.lastName;
    }

}

var john = {
    firstName: 'john',
    lastName: 'doe'
}


// DO not ever do this. just for demo

john.__proto__ = personA;
console.log(john.getFullName())

console.log(john.firstName);

var jane = {
    firstName: 'jane'
}

jane.__proto__ = personA;
console.log(jane.getFullName());

console.log('everything is an object');
var a ={};
var b = function(){

}
var c= [];

console.log(a.__proto__);

console.log(b.__proto__.__proto__);


console.log(c.__proto__.__proto__);



console.log('learn about reflection and extend')

for(var prop in john){
  //  console.log(prop+ ':' + john[prop]);

  if(john.hasOwnProperty(prop)){
    console.log(prop+ ':' + john[prop]);
  }
}

var jane = {
    address:'111 main status.',
    getFormalFullName:  function(){
        return this.lastName+ ' ' + this.firstName;
    }

}

var jim = {
    getFirstName: function(){
        return firstName;
    }
}

_.extend(john,jane,jim);

console.log(john);