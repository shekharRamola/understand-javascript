console.log('pure protypical inheritence');


var obj = {
    firstname:'default',
    lastname:'default',
    fullname: function(){
        return this.firstname+' '+this.lastname;
    }
}

var obj1 = Object.create(obj);
console.log(obj1.fullname());

obj1.firstname = 'Shekhar';
obj1.lastname = 'Ramola';
console.log(obj1.fullname());
