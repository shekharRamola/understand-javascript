//Understand scope chain


// here function b is sitting lexically at the global level, same level as myVar, not inside a function. so it will crate an outer lexical environment reference to the the outer lexical environment. If b will sit physically inside a function then it's outer lexical environment will be a.
function b(){
    console.log(myvar)
}

function a(){
    var myvar = 2;
    b();
}

var myvar =1;
a();

function foundOccurance(){
    
    
}


function convert(array){  
    let obj ={};
    for( let i=0; i< array.length; i++) {
        let count =0;
        for(let j= 0; j<array.length; j++){
            if(array[j] === array[i]){
                count++;
            }

        }

        obj[array[i]] = count;

    }
    return obj;
}


console.log(convert([0,2,3,3,3,4,4,5]));



function countByreduce(array){
  return array.reduce(function (acc, curr) {
  if (typeof acc[curr] == 'undefined') {
    acc[curr] = 1;
  } else {
    acc[curr] += 1;
  }

  return acc;
}, {});
}

console.log(countByreduce([0,2,3,3,3,4,4,5]));